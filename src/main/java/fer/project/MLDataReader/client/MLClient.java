package fer.project.MLDataReader.client;

import fer.project.MLDataReader.domain.Labels;
import fer.project.MLDataReader.domain.RequestFeatures;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;

@FeignClient(name = "MLClient", url = "${app.ml.api-url}")
public interface MLClient {

    @PostMapping(value = "/predict")
    ResponseEntity<Labels> getPrediction(RequestFeatures requestFeatures);

}
