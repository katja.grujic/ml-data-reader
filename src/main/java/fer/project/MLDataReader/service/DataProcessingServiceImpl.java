package fer.project.MLDataReader.service;

import feign.FeignException;
import fer.project.MLDataReader.client.MLClient;
import fer.project.MLDataReader.domain.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DataProcessingServiceImpl implements DataProcessingService {

    private MLClient client;

    @Autowired
    public DataProcessingServiceImpl(MLClient client) {
        this.client = client;
    }

    @Override
    public FeaturesLabels processData(String line) {
        Features features = new Features(line);

        StallingFeature stalling = new StallingFeature(features);
        BitrateFeature bitrate = new BitrateFeature(features);
        ResolutionFeature resolution = new ResolutionFeature(features);

        RequestFeatures request = new RequestFeatures(stalling, bitrate, resolution);

        Labels labels;
        labels = client.getPrediction(request).getBody();

        FeaturesLabels featuresLabels = new FeaturesLabels();
        featuresLabels.setFeatures(features);
        featuresLabels.setLabels(labels);

        return featuresLabels;
    }
}
