package fer.project.MLDataReader.service;

import fer.project.MLDataReader.domain.FeaturesLabels;

public interface DataProcessingService {

    FeaturesLabels processData(String line);
}
