package fer.project.MLDataReader.domain;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Features {

    @JsonProperty("DLrate_w1")
     private double DLrate_w1;

    @JsonProperty("DLrate_wl5")
     private double DLrate_wl5;

    @JsonProperty("DLrate_wl10")
     private double DLrate_wl10;

    @JsonProperty("DLrate_wl20")
     private double DLrate_wl20;

    @JsonProperty("DLrate_wl100")
     private double DLrate_wl100;

    @JsonProperty("DLload_w1")
     private double DLload_w1;

    @JsonProperty("DLload_wl5")
     private double DLload_wl5;

    @JsonProperty("DLload_wl10")
     private double DLload_wl10;

    @JsonProperty("DLload_wl20")
     private double DLload_wl20;

    @JsonProperty("DLload_wl100")
     private double DLload_wl100;

    @JsonProperty("ULnPckts_w1")
     private int ULnPckts_w1;

    @JsonProperty("ULnPckts_wl5")
     private int ULnPckts_wl5;

    @JsonProperty("ULnPckts_wl10")
     private int ULnPckts_wl10;

    @JsonProperty("ULnPckts_wl20")
     private int ULnPckts_wl20;

    @JsonProperty("ULnPckts_wl100")
     private int ULnPckts_wl100;

    @JsonProperty("ULavgSize_w1")
     private double ULavgSize_w1;

    @JsonProperty("ULavgSize_wl5")
     private double ULavgSize_wl5;

    @JsonProperty("ULavgSize_wl10")
     private double ULavgSize_wl10;

    @JsonProperty("ULavgSize_wl20")
     private double ULavgSize_wl20;

    @JsonProperty("ULavgSize_wl100")
     private double ULavgSize_wl100;

    @JsonProperty("ULstdSize_w1")
     private double ULstdSize_w1;

    @JsonProperty("ULstdSize_wl5")
     private double ULstdSize_wl5;

    @JsonProperty("ULstdSize_wl10")
     private double ULstdSize_wl10;

    @JsonProperty("ULstdSize_wl20")
     private double ULstdSize_wl20;

    @JsonProperty("ULstdSize_wl100")
     private double ULstdSize_wl100;

    public Features(String line) {
            String[] values = line.split(",");
            this.DLrate_w1 = Double.parseDouble(values[2]);
            this.DLrate_wl5 = Double.parseDouble(values[3]);
            this.DLrate_wl10 = Double.parseDouble(values[4]);
            this.DLrate_wl20 = Double.parseDouble(values[5]);
            this.DLrate_wl100 = Double.parseDouble(values[6]);
            this.DLload_w1 = Double.parseDouble(values[7]);
            this.DLload_wl5 = Double.parseDouble(values[8]);
            this.DLload_wl10 = Double.parseDouble(values[9]);
            this.DLload_wl20 = Double.parseDouble(values[10]);
            this.DLload_wl100 = Double.parseDouble(values[11]);
            this.ULnPckts_w1 = Integer.parseInt(values[12]);
            this.ULnPckts_wl5 = Integer.parseInt(values[13]);
            this.ULnPckts_wl10 = Integer.parseInt(values[14]);
            this.ULnPckts_wl20 = Integer.parseInt(values[15]);
            this.ULnPckts_wl100 = Integer.parseInt(values[16]);
            this.ULavgSize_w1 = Double.parseDouble(values[17]);
            this.ULavgSize_wl5 = Double.parseDouble(values[18]);
            this.ULavgSize_wl10 = Double.parseDouble(values[19]);
            this.ULavgSize_wl20 = Double.parseDouble(values[20]);
            this.ULavgSize_wl100 = Double.parseDouble(values[21]);
            this.ULstdSize_w1 = Double.parseDouble(values[22]);
            this.ULstdSize_wl5 = Double.parseDouble(values[23]);
            this.ULstdSize_wl10 = Double.parseDouble(values[24]);
            this.ULstdSize_wl20 = Double.parseDouble(values[25]);
            this.ULstdSize_wl100 = Double.parseDouble(values[26]);
    }

    public double getDLrate_w1() {
        return DLrate_w1;
    }

    public void setDLrate_w1(double DLrate_w1) {
        this.DLrate_w1 = DLrate_w1;
    }

    public double getDLrate_wl5() {
        return DLrate_wl5;
    }

    public void setDLrate_wl5(double DLrate_wl5) {
        this.DLrate_wl5 = DLrate_wl5;
    }

    public double getDLrate_wl10() {
        return DLrate_wl10;
    }

    public void setDLrate_wl10(double DLrate_wl10) {
        this.DLrate_wl10 = DLrate_wl10;
    }

    public double getDLrate_wl20() {
        return DLrate_wl20;
    }

    public void setDLrate_wl20(double DLrate_wl20) {
        this.DLrate_wl20 = DLrate_wl20;
    }

    public double getDLrate_wl100() {
        return DLrate_wl100;
    }

    public void setDLrate_wl100(double DLrate_wl100) {
        this.DLrate_wl100 = DLrate_wl100;
    }

    public double getDLload_w1() {
        return DLload_w1;
    }

    public void setDLload_w1(double DLload_w1) {
        this.DLload_w1 = DLload_w1;
    }

    public double getDLload_wl5() {
        return DLload_wl5;
    }

    public void setDLload_wl5(double DLload_wl5) {
        this.DLload_wl5 = DLload_wl5;
    }

    public double getDLload_wl10() {
        return DLload_wl10;
    }

    public void setDLload_wl10(double DLload_wl10) {
        this.DLload_wl10 = DLload_wl10;
    }

    public double getDLload_wl20() {
        return DLload_wl20;
    }

    public void setDLload_wl20(double DLload_wl20) {
        this.DLload_wl20 = DLload_wl20;
    }

    public double getDLload_wl100() {
        return DLload_wl100;
    }

    public void setDLload_wl100(double DLload_wl100) {
        this.DLload_wl100 = DLload_wl100;
    }

    public int getULnPckts_w1() {
        return ULnPckts_w1;
    }

    public void setULnPckts_w1(int ULnPckts_w1) {
        this.ULnPckts_w1 = ULnPckts_w1;
    }

    public int getULnPckts_wl5() {
        return ULnPckts_wl5;
    }

    public void setULnPckts_wl5(int ULnPckts_wl5) {
        this.ULnPckts_wl5 = ULnPckts_wl5;
    }

    public int getULnPckts_wl10() {
        return ULnPckts_wl10;
    }

    public void setULnPckts_wl10(int ULnPckts_wl10) {
        this.ULnPckts_wl10 = ULnPckts_wl10;
    }

    public int getULnPckts_wl20() {
        return ULnPckts_wl20;
    }

    public void setULnPckts_wl20(int ULnPckts_wl20) {
        this.ULnPckts_wl20 = ULnPckts_wl20;
    }

    public int getULnPckts_wl100() {
        return ULnPckts_wl100;
    }

    public void setULnPckts_wl100(int ULnPckts_wl100) {
        this.ULnPckts_wl100 = ULnPckts_wl100;
    }

    public double getULavgSize_w1() {
        return ULavgSize_w1;
    }

    public void setULavgSize_w1(double ULavgSize_w1) {
        this.ULavgSize_w1 = ULavgSize_w1;
    }

    public double getULavgSize_wl5() {
        return ULavgSize_wl5;
    }

    public void setULavgSize_wl5(double ULavgSize_wl5) {
        this.ULavgSize_wl5 = ULavgSize_wl5;
    }

    public double getULavgSize_wl10() {
        return ULavgSize_wl10;
    }

    public void setULavgSize_wl10(double ULavgSize_wl10) {
        this.ULavgSize_wl10 = ULavgSize_wl10;
    }

    public double getULavgSize_wl20() {
        return ULavgSize_wl20;
    }

    public void setULavgSize_wl20(double ULavgSize_wl20) {
        this.ULavgSize_wl20 = ULavgSize_wl20;
    }

    public double getULavgSize_wl100() {
        return ULavgSize_wl100;
    }

    public void setULavgSize_wl100(double ULavgSize_wl100) {
        this.ULavgSize_wl100 = ULavgSize_wl100;
    }

    public double getULstdSize_w1() {
        return ULstdSize_w1;
    }

    public void setULstdSize_w1(double ULstdSize_w1) {
        this.ULstdSize_w1 = ULstdSize_w1;
    }

    public double getULstdSize_wl5() {
        return ULstdSize_wl5;
    }

    public void setULstdSize_wl5(double ULstdSize_wl5) {
        this.ULstdSize_wl5 = ULstdSize_wl5;
    }

    public double getULstdSize_wl10() {
        return ULstdSize_wl10;
    }

    public void setULstdSize_wl10(double ULstdSize_wl10) {
        this.ULstdSize_wl10 = ULstdSize_wl10;
    }

    public double getULstdSize_wl20() {
        return ULstdSize_wl20;
    }

    public void setULstdSize_wl20(double ULstdSize_wl20) {
        this.ULstdSize_wl20 = ULstdSize_wl20;
    }

    public double getULstdSize_wl100() {
        return ULstdSize_wl100;
    }

    public void setULstdSize_wl100(double ULstdSize_wl100) {
        this.ULstdSize_wl100 = ULstdSize_wl100;
    }
}
