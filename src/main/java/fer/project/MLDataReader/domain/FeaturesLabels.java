package fer.project.MLDataReader.domain;

public class FeaturesLabels {

    private Features features;
    private Labels labels;

    public Features getFeatures() {
        return features;
    }

    public void setFeatures(Features features) {
        this.features = features;
    }

    public Labels getLabels() {
        return labels;
    }

    public void setLabels(Labels labels) {
        this.labels = labels;
    }
}
