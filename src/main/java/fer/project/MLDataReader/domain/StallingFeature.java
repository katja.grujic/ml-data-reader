package fer.project.MLDataReader.domain;

public class StallingFeature {
    private double ULstdSize_wl20;
    private double DLload_wl100;
    private double DLload_w1;
    private double DLload_wl5;
    private double DLrate_wl10;
    private double DLrate_wl5;
    private double DLload_wl20;
    private double DLrate_w1;
    private int ULnPckts_wl20;
    private double DLload_wl10;

    public StallingFeature(Features features) {
        this.ULstdSize_wl20 = features.getULstdSize_wl20();
        this.DLload_wl100 = features.getDLload_wl100();
        this.DLload_w1 = features.getDLload_w1();
        this.DLload_wl5 = features.getDLload_wl5();
        this.DLrate_wl10 = features.getDLrate_wl10();
        this.DLrate_wl5 = features.getDLrate_wl5();
        this.DLload_wl20 = features.getDLload_wl20();
        this.DLrate_w1 = features.getDLrate_w1();
        this.ULnPckts_wl20 = features.getULnPckts_wl20();
        this.DLload_wl10 = features.getDLload_wl10();
    }

    public StallingFeature() {
    }

    public double getULstdSize_wl20() {
        return ULstdSize_wl20;
    }

    public void setULstdSize_wl20(double ULstdSize_wl20) {
        this.ULstdSize_wl20 = ULstdSize_wl20;
    }

    public double getDLload_wl100() {
        return DLload_wl100;
    }

    public void setDLload_wl100(double DLload_wl100) {
        this.DLload_wl100 = DLload_wl100;
    }

    public double getDLload_w1() {
        return DLload_w1;
    }

    public void setDLload_w1(double DLload_w1) {
        this.DLload_w1 = DLload_w1;
    }

    public double getDLload_wl5() {
        return DLload_wl5;
    }

    public void setDLload_wl5(double DLload_wl5) {
        this.DLload_wl5 = DLload_wl5;
    }

    public double getDLrate_wl10() {
        return DLrate_wl10;
    }

    public void setDLrate_wl10(double DLrate_wl10) {
        this.DLrate_wl10 = DLrate_wl10;
    }

    public double getDLrate_wl5() {
        return DLrate_wl5;
    }

    public void setDLrate_wl5(double DLrate_wl5) {
        this.DLrate_wl5 = DLrate_wl5;
    }

    public double getDLload_wl20() {
        return DLload_wl20;
    }

    public void setDLload_wl20(double DLload_wl20) {
        this.DLload_wl20 = DLload_wl20;
    }

    public double getDLrate_w1() {
        return DLrate_w1;
    }

    public void setDLrate_w1(double DLrate_w1) {
        this.DLrate_w1 = DLrate_w1;
    }

    public int getULnPckts_wl20() {
        return ULnPckts_wl20;
    }

    public void setULnPckts_wl20(int ULnPckts_wl20) {
        this.ULnPckts_wl20 = ULnPckts_wl20;
    }

    public double getDLload_wl10() {
        return DLload_wl10;
    }

    public void setDLload_wl10(double DLload_wl10) {
        this.DLload_wl10 = DLload_wl10;
    }
}
