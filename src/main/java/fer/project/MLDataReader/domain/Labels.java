package fer.project.MLDataReader.domain;

public class Labels {

    private String stalling;
    private String bitrate;
    private String resolution;

    public String getStalling() {
        return stalling;
    }

    public void setStalling(String stalling) {
        this.stalling = stalling;
    }

    public String getBitrate() {
        return bitrate;
    }

    public void setBitrate(String bitrate) {
        this.bitrate = bitrate;
    }

    public String getResolution() {
        return resolution;
    }

    public void setResolution(String resolution) {
        this.resolution = resolution;
    }
}
