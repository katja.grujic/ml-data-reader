package fer.project.MLDataReader.domain;

public class RequestFeatures {

    private StallingFeature stalling;
    private BitrateFeature bitrate;
    private ResolutionFeature resolution;

    public RequestFeatures(StallingFeature stalling, BitrateFeature bitrate, ResolutionFeature resolution) {
        this.stalling = stalling;
        this.bitrate = bitrate;
        this.resolution = resolution;
    }

    public StallingFeature getStalling() {
        return stalling;
    }

    public void setStalling(StallingFeature stalling) {
        this.stalling = stalling;
    }

    public BitrateFeature getBitrate() {
        return bitrate;
    }

    public void setBitrate(BitrateFeature bitrate) {
        this.bitrate = bitrate;
    }

    public ResolutionFeature getResolution() {
        return resolution;
    }

    public void setResolution(ResolutionFeature resolution) {
        this.resolution = resolution;
    }
}
