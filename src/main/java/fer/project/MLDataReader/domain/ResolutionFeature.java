package fer.project.MLDataReader.domain;

public class ResolutionFeature {
    private double DLrate_wl10;
    private double DLload_wl5;
    private double DLload_wl20;
    private int ULnPckts_wl100;
    private double DLrate_wl20;
    private double ULstdSize_wl100;
    private double DLload_wl10;
    private double ULavgSize_wl100;
    private double DLrate_wl100;
    private double DLload_wl100;

    public ResolutionFeature(Features features) {
        this.DLrate_wl10 = features.getDLrate_wl10();
        this.DLload_wl5 = features.getDLload_wl5();
        this.DLload_wl20 = features.getDLload_wl20();
        this.ULnPckts_wl100 = features.getULnPckts_wl100();
        this.DLrate_wl20 = features.getDLrate_wl20();
        this.ULstdSize_wl100 = features.getULstdSize_wl100();
        this.DLload_wl10 = features.getDLload_wl10();
        this.ULavgSize_wl100 = features.getULavgSize_wl100();
        this.DLrate_wl100 = features.getDLrate_wl100();
        this.DLload_wl100 = features.getDLload_wl100();
    }

    public ResolutionFeature() {
    }

    public double getDLrate_wl10() {
        return DLrate_wl10;
    }

    public void setDLrate_wl10(double DLrate_wl10) {
        this.DLrate_wl10 = DLrate_wl10;
    }

    public double getDLload_wl5() {
        return DLload_wl5;
    }

    public void setDLload_wl5(double DLload_wl5) {
        this.DLload_wl5 = DLload_wl5;
    }

    public double getDLload_wl20() {
        return DLload_wl20;
    }

    public void setDLload_wl20(double DLload_wl20) {
        this.DLload_wl20 = DLload_wl20;
    }

    public int getULnPckts_wl100() {
        return ULnPckts_wl100;
    }

    public void setULnPckts_wl100(int ULnPckts_wl100) {
        this.ULnPckts_wl100 = ULnPckts_wl100;
    }

    public double getDLrate_wl20() {
        return DLrate_wl20;
    }

    public void setDLrate_wl20(double DLrate_wl20) {
        this.DLrate_wl20 = DLrate_wl20;
    }

    public double getULstdSize_wl100() {
        return ULstdSize_wl100;
    }

    public void setULstdSize_wl100(double ULstdSize_wl100) {
        this.ULstdSize_wl100 = ULstdSize_wl100;
    }

    public double getDLload_wl10() {
        return DLload_wl10;
    }

    public void setDLload_wl10(double DLload_wl10) {
        this.DLload_wl10 = DLload_wl10;
    }

    public double getULavgSize_wl100() {
        return ULavgSize_wl100;
    }

    public void setULavgSize_wl100(double ULavgSize_wl100) {
        this.ULavgSize_wl100 = ULavgSize_wl100;
    }

    public double getDLrate_wl100() {
        return DLrate_wl100;
    }

    public void setDLrate_wl100(double DLrate_wl100) {
        this.DLrate_wl100 = DLrate_wl100;
    }

    public double getDLload_wl100() {
        return DLload_wl100;
    }

    public void setDLload_wl100(double DLload_wl100) {
        this.DLload_wl100 = DLload_wl100;
    }
}
