package fer.project.MLDataReader;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients
public class MlDataReaderApplication {

	public static void main(String[] args) {
		SpringApplication.run(MlDataReaderApplication.class, args);
	}

}
