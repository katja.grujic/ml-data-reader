package fer.project.MLDataReader.controller;

import fer.project.MLDataReader.domain.FeaturesLabels;
import fer.project.MLDataReader.service.DataProcessingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

@RestController
@CrossOrigin
@RequestMapping("/data")
public class DataController {

    private DataProcessingService dataProcessingService;
    private BufferedReader reader;

    @Autowired
    public DataController(DataProcessingService dataProcessingService) {
        this.dataProcessingService = dataProcessingService;
    }

    @PostMapping(value = "/upload", consumes = "multipart/form-data;charset=UTF-8")
    public ResponseEntity<Void> updateFile(@RequestParam("csvFile") MultipartFile csvFile) {
        System.out.println("Got input file.");
        try {
            System.out.println("Trying to make input stream.");
            InputStream is = csvFile.getInputStream();
            reader = new BufferedReader(new InputStreamReader(is));
        } catch (Exception e) {
            System.err.println(e.getMessage());
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        System.out.println("File successfully uploaded.");
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping(value = "/read")
    public ResponseEntity<FeaturesLabels> readLine() {
        FeaturesLabels response;
        try {
            System.out.println("Trying to read data.");
            String line = reader.readLine();
            if (line != null) {
                response = dataProcessingService.processData(line);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            System.err.println(e.getMessage());
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        return ResponseEntity.ok(response);
    }
}